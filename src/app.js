const express = require('express');
const app = express();
const path = require('path');
const morgan = require('morgan');
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, 	X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-	Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, 	DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});
// settings
app.set('port', 5500);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Middlewares
// funciones que se ejecutan antes de que lleguen a la ruta
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));

// Routes
app.use(require('./routes/index'));

// Static
app.use(express.static(path.join(__dirname, 'public')));

//404 handler
app.use((req, res, next) => {
    res.status(404).send('404 Not found');
});

module.exports = app;