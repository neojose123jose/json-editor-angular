const { Router } = require("express");
const router = Router();
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const json_books = fs.readFileSync("src/books.json", "utf-8");
let books = JSON.parse(json_books);

const historyBooks = fs.readFileSync("src/historyRoutes.json", "utf-8");
let bookHistory = JSON.parse(historyBooks);

let deleteArray = [];
let modify = [];
let IdAEditar = "";
let idFatherToSaveInSon = "";



router.get("/", (req, res) => {
  result = books.filter((book) => book.idFather == "");
  result.forEach(function (value, index) {
    value.numero = ++index;
  });
  res.send(result);
  res.render("index.ejs", {
    result,
  });
});

router.get("/new-entry", (req, res) => {
  res.render("new-entry");
});

router.post("/new-entry", (req, res) => {
  const { message, opcion, respond, result } = req.body;
  if (!message || !opcion || !respond) {
    res.status(400).send("Entradas por validar");
    return;
  }
  let newBooks = {
    id: uuidv4(),
    message,
    opcion,
    idFather: "",
    respond,
    result,
  };
  //res.send(newBooks);
  books.push(newBooks);
  const json_books = JSON.stringify(books);
  fs.writeFileSync("src/books.json", json_books, "utf-8");
  res.redirect("/");
});

router.get("/delete/:id", (req, res) => {
  //deleteArray = books;
  //var prueba = req.params.id;
  //  deleteArray = deleteArray.filter((book) => book.id != prueba);
  result = books.filter((book) => book.id != req.params.id);
  modify = JSON.stringify(result);
  fs.writeFileSync("src/books.json", modify, "utf-8");
  //res.redirect("/");
});

router.get("/edit/:id", (req, res) => {
  IdAEditar = req.params.id;
  result = books.filter((book) => book.id == req.params.id);
  //res.send(result[0].title);
  res.render("edit.ejs", {
    result,
  });
});

router.post("/editar", (req, res) => {
  //res.send(req.body);
  let idIndex = 0;
  books.forEach(function (value, index) {
    if (value.id == IdAEditar) {
      idIndex = index;
    }
  });

  let newBooks = {
    id: books[idIndex].id,
    message: req.body.message,
    opcion: req.body.opcion,
    idFather: books[idIndex].idFather,
    respond: req.body.respond,
    result: req.body.result,
  };

  books[idIndex] = newBooks;
  const json_books = JSON.stringify(books);
  fs.writeFileSync("src/books.json", json_books, "utf-8");
  verificacionPadrePrincipal = books.filter((book) => book.id == bookHistory.route);
  //res.send(verificacionPadrePrincipal[0]);

  /*   if (verificacionPadrePrincipal[0].idFather === "") {
      res.redirect("/");
    } */

  bookHistory.rutaDirigir = bookHistory.route;
  //bookHistory.route = '';
  res.redirect("/viewSon/" + bookHistory.rutaDirigir);

  //books.push(newBooks);
  //console.log(books);
});

router.get("/viewSon/:id", (req, res) => {
  IdAEditar = req.params.id;
  idFatherToSaveInSon = req.params.id;
  let history = {
    route: IdAEditar,
    edit: "",
  };
  const json_books = JSON.stringify(history);
  fs.writeFileSync("src/historyRoutes.json", json_books, "utf-8");
  nombreDelPadre = books.filter((book) => book.id == req.params.id);
  nombreDelPadre = nombreDelPadre[0].opcion;
  result = books.filter((book) => book.idFather == req.params.id);

  result.forEach(function (value, index) {
    value.numero = ++index;
  });
  res.render("viewSon.ejs", {
    result, IdAEditar, nombreDelPadre,
  });
});



router.get("/createSon", (req, res) => {
  res.render("createSon.ejs");
});

router.post("/createSon", (req, res) => {
  const { message, opcion, respond, result } = req.body;
  if (!message || !opcion || !respond) {
    res.status(400).send("Entradas por validar");
    return;
  }
  let newBooks = {
    id: uuidv4(),
    message,
    opcion,
    idFather: bookHistory.route,
    respond,
    result,
  };
  //res.send(newBooks);
  books.push(newBooks);
  const json_books = JSON.stringify(books);
  fs.writeFileSync("src/books.json", json_books, "utf-8");

  bookHistory.rutaDirigir = bookHistory.route;
  bookHistory.route = '';
  res.redirect('/viewSon/' + bookHistory.rutaDirigir);

});

module.exports = router;
